Really simple plugin to make the backgrounds
of active and inactive windows different.

No configuration, to change the colours,
edit the plugin directly.
