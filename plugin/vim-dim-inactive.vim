" if g:colors_name == 'myscheme'
"     hi Normal guibg=#142227
"     hi ActiveWindow guibg=#142227
"     hi InactiveWindow guibg=#0D0B02
" else
    hi Normal guibg=#17252c
    hi InactiveWindow guibg=#17252c
    hi ActiveWindow guibg=#0D0B02
" endif

augroup WindowManagement
  autocmd!
  autocmd WinEnter * call Handle_Win_Enter()
augroup END

" Change highlight group of active/inactive windows
function! Handle_Win_Enter()
  setlocal winhighlight=Normal:ActiveWindow,NormalNC:InactiveWindow
endfunction
setlocal winhighlight=Normal:ActiveWindow,NormalNC:InactiveWindow
